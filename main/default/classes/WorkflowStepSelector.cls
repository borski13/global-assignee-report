/**
 * @File Name          : WorkflowRuleSelector.cls
 * @Description        : 
 * @Author             : Jon Astemborski
 * @Group              : 
 * @Last Modified By   : Jon Astemborski
 * @Last Modified On   : 1/30/2020, 11:44:26 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/24/2020   Jon Astemborski     Initial Version
**/
public class WorkflowStepSelector extends fflib_SObjectSelector{
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            CMPL123__WF_Step__c.Id, 
            CMPL123__WF_Step__c.CMPL123__WF_Rule__c,
            CMPL123__WF_Step__c.CMPL123__Permission_Group__c};
    }
    public Schema.SObjectType getSObjectType() {
        return CMPL123__WF_Step__c.sObjectType;
    }
    public List<CMPL123__WF_Step__c> selectById(Set<ID> idSet) {
        return (List<CMPL123__WF_Step__c>) selectSObjectsById(idSet);
    }

    public List<CMPL123__WF_Step__c> selectAll() {   
        return (List<CMPL123__WF_Step__c>) Database.query(
        /**
          Query factory has been pre-initialised by calling
          getSObjectFieldList(), getOrderBy() for you.
        */
        newQueryFactory().
        selectField('CMPL123__WF_Rule__c.CMPL123__Target_Object__c').
        selectField('CMPL123__End_Status__r.Name').
        selectField('CMPL123__Start_Status__r.Name').

        /**
          Now focus on building the remainder of the
          query needed for this method.
        */
        toSOQL());
    }
    
}


@isTest
private class SoqlQueryTest {

    static List<String> selectionCriteria;
    static Map<String,List<String>> whereCriteriaMap;
    static List<String> whereCriteriaList;
    static Product_Information__c prodInfo;
    static SoqlQuery soql1;
    static SoqlQuery soql2;
	
	@isTest static void testToString() {
		selectionCriteria = new List<String>();
		selectionCriteria.add('ID');
		selectionCriteria.add('Name');
    	whereCriteriaMap = new Map<String,List<String>>();
    	whereCriteriaList = new List<String>();
    	whereCriteriaList.add('123, 234');
    	whereCriteriaMap.put('ID', whereCriteriaList);
        soql1 = new SoqlQuery(selectionCriteria, 'TEST', whereCriteriaMap, false);
        String expected1 = 'SELECT ID,Name FROM TEST WHERE ID IN (\'123, 234\')';
        //system.assertEquals(expected1.toUpperCase().trim(),soql1.toString().toUpperCase().trim());
	}

	@isTest static void testToStringNested() 
	{
		selectionCriteria = new List<String>();
		selectionCriteria.add('ID');
		selectionCriteria.add('Name');
	 	String nestedQuery = 'SELECT ID FROM TEST_2';
        whereCriteriaList =  new List<String>();
        whereCriteriaMap = new Map<String,List<String>>();
        whereCriteriaList.add(nestedQuery);
        whereCriteriaMap.put('ID', whereCriteriaList);
        soql2 = new SoqlQuery(selectionCriteria, 'TEST', whereCriteriaMap, true);
        String expected2 = 'SELECT ID,Name FROM TEST WHERE ID IN (SELECT ID FROM TEST_2)';
        //system.assertEquals(expected2.toUpperCase().trim(), soql2.toString().toUpperCase().trim());
	}
	

}
/*** Controller: ***/

public class AssignedUserReportController {

    public List<AssignedUserMembership> assignedRecords;
    public String userName = '0054T0000014GDn';
    public Map<String, List<String>> recordMap;
    public String JsonMap{get;set;} 


    public List<AssignedUserMembership> getAssignedRecords() {
        List<sObject> rawRecords = retrieveAssigneeRecords('CAPA_tw__c');
        List<AssignedUserMembership> assigneeMembershipList = convertToAssigneeUserMembersup(rawRecords);
        return assigneeMembershipList;
    }
    
    public PageReference exportToExcel(){
        PageReference retURLExcel = new PageReference('/apex/GlobalAssignedUserReport');
        return retURLExcel;
    }

    public List<sObject> retrieveAssigneeRecords(String objectName){
        SoqlQuery query = new SoqlQuery();
        List<String> selectionList = new List<String>();
        selectionList.add('Id');
        selectionList.add('CMPL123_WF_Status__c');
        selectionList.add('LastModifiedDate');
        List<String> assigneeFields = retrieveAssigneeFields(objectName);
        System.debug('Assignee Fields: ' + assigneeFields);
        selectionList.addAll(assigneeFields);
        List<SoqlQuery.WhereCriterium> whereCriteriaList = generateWhereCriteria(assigneeFields);
        SoqlQuery soqlQuery = new SoqlQuery(selectionList, objectName, whereCriteriaList);
        List<sObject> assigneeRecords = Database.query(soqlQuery.generateBasicSoqlQuery());
        return assigneeRecords;
    }

    public List<String> retrieveAssigneeFields(String objectName){
        User_Workflow_Report__mdt workflowReport = [Select id, Field_List__c from User_Workflow_Report__mdt where Object_Name__c =:objectName];
        String rawFields = workflowReport.Field_List__c;
        return rawFields.split(',');
    }

    public List<AssignedUserMembership> convertToAssigneeUserMembersup( List<sObject> recordList ){
       List<AssignedUserMembership> assignedRecords = new List<AssignedUserMembership>();
       for (SObject obj : recordList){
           AssignedUserMembership membership = new AssignedUserMembership();
           membership.recordId = obj.get('Id').toString();
           membership.workflowStatus = obj.get('CMPL123_WF_Status__c').toString();
           membership.lastModifiedDate = obj.get('lastModifiedDate').toString();
           assignedRecords.add(membership);
       }
       return assignedRecords;

    }

    public List<SoqlQuery.WhereCriterium> generateWhereCriteria(List<String> assigneeFields){
        List<SoqlQuery.WhereCriterium> whereCriteriaList = new List<SoqlQuery.WhereCriterium>();
        for (String assigneeField : assigneeFields) {
            SoqlQuery.WhereCriterium whereCriteria = new SoqlQuery.WhereCriterium();
            whereCriteria.fieldApiName = assigneeField;
            SoqlQuery.WhereTuple whereTuple = new SoqlQuery.WhereTuple();
            whereTuple.isNegation = false;
            List<String> whereinlist = new List<String>();
            whereinList.add(userName);
            whereTuple.whereInList = whereInList;
            whereCriteria.whereTuple = whereTuple;
            whereCriteriaList.add(whereCriteria);
        }
        return whereCriteriaList;
    }

}
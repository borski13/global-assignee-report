public without sharing class AssignedUserMembership {
    public String userName {get;set;}
    public String fieldName {get;set;}
    public String recordId {get;set;}
    public String workflowStatus {get;set;}
    public String lastModifiedDate {get;set;}

    public AssignedUserMembership() {
    }
}

/**
 * @File Name          : WorkflowRuleSelector.cls
 * @Description        : 
 * @Author             : Jon Astemborski
 * @Group              : 
 * @Last Modified By   : Jon Astemborski
 * @Last Modified On   : 1/30/2020, 9:06:34 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/24/2020   Jon Astemborski     Initial Version
**/
public class ActionSelector extends fflib_SObjectSelector{
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            Action_Item__c.Id
        };
    }
    public Schema.SObjectType getSObjectType() {
        return Action_Item__c.sObjectType;
    }
    public List<Action_Item__c> selectById(Set<ID> idSet) {
        return (List<Action_Item__c>) selectSObjectsById(idSet);
    }

    public List<Action_Item__c> selectAll() {   
        return (List<Action_Item__c>) Database.query(
        /**
          Query factory has been pre-initialised by calling
          getSObjectFieldList(), getOrderBy() for you.
        */
        newQueryFactory().
        selectField('Action_Item__c.CMPL123__Target_Object__c').

        /**
          Now focus on building the remainder of the
          query needed for this method.
        */
        toSOQL());
    }

//List<String> userIds
    public String getUserField(){
        Schema.SObjectType type = this.getSObjectType();
        Map<String, Schema.SObjectField> schemaFieldMap = Schema.SObjectType.Action_Item__c.fields.getMap();
        for (String fieldName: schemaFieldMap.keySet()) {
            Schema.DescribeFieldResult field = schemaFieldMap.get(fieldName).getDescribe();
            if ( String.valueof(field.getType()).equals('REFERENCE') && String.valueof(field.getreferenceto()).toUpperCase().contains('USER')){
                system.debug(field.getName());
                system.debug(field.getReferenceTo());
            }
        }
        return '';
    }

    public List<Action_Item__c> selectRecentlyUpdated(List<String> userIds) {
    String query = String.format(
    'select {0} from {1} ' +
    'where {2} ',
    new List<String> {
        getFieldListString(),
        getSObjectName(),
        getUserField()
      }
    );
    return (List<Action_Item__c>) Database.query(query);
}

    
    
}


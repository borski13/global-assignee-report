/**
 * @File Name          : GlobalAssignedReportSetupController.cls
 * @Description        : 
 * @Author             : Jon Astemborski
 * @Group              : 
 * @Last Modified By   : Jon Astemborski
 * @Last Modified On   : 2/4/2020, 3:18:52 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/23/2020   Jon Astemborski     Initial Version
 **/
public class GlobalAssignedReportSetupController {

    private static final String WF_STEP_USER_FIELD_PREFIX = '$';
    public class wfWrapper {
        public String apiName {get;set;}
        public String label {get;set;}
    }

    public List < wfWrapper > wfObjects {get;set;}
    public List <Global_Assignee_Report_Setup__c> reportTypeConfig {get;set;}
    public String selectedApi {get;set;}
    public Map < String, List < String >> objectFieldMapping {get;set;}
    public Map < String, Set < String >> objectWfMapping {get;set;}
    public String JsonMap {get;set;}
    public String wfStatusMap {get;set;}
    public String reportConfigJson {get;set;}

    public GlobalAssignedReportSetupController() {
        selectedApi = '';
        wfObjects = new List < wfWrapper > ();
        objectFieldMapping = new Map < String, List < String >> ();
        objectWfMapping = new Map < String, Set < String >> ();
        JsonMap = '';
        wfStatusMap = '';
        reportConfigJson = '';
        objectFieldMapping.put(selectedApi, new List < String > ());
        objectWfMapping.put(selectedApi, new Set < String > ());
        reportTypeConfig = new List<Global_Assignee_Report_Setup__c>();

        List < String > objectsWithWorkflows = new List < String > ();
        List < CMPL123__WF_Rule__c > wfRules = new WorkflowRuleSelector().selectAll();
        List < CMPL123__WF_Step__c > wfSteps = new WorkflowStepSelector().selectAll();

        Map < String, List < String >> wfRuleWrapper = new Map < String, List < String >> ();
        Map < String, Set < String >> wfStatusWrapper = new Map < String, Set < String >> ();
        
        Map < String, List<String> > objectIdNameMap = new Map < String, List<String> > ();
            List<String> test1 = new List<String>();

        for (CMPL123__WF_Rule__c rule: wfRules) {
            objectsWithWorkflows.add(rule.CMPL123__Target_Object__c);
            List<String> test = new List<String>();
            if ( objectIdNameMap.containsKey(rule.id)){
                test.addAll(objectIdNameMap.get(rule.id));
            }
            test.add(rule.CMPL123__Target_Object__c);
            objectIdNameMap.put(rule.id, test);
            test1.add(rule.CMPL123__Target_Object__c);
        }

        for (CMPL123__WF_Step__c step: wfSteps) {
            List < String > tempList = new List < String > ();
            Set <String> tempStatusSet = new Set <String> ();
            if (wfRuleWrapper.containsKey(step.CMPL123__WF_Rule__r.CMPL123__Target_Object__c)) {
                tempList.addAll(wfRuleWrapper.get(step.CMPL123__WF_Rule__r.CMPL123__Target_Object__c));
            }
            if ( wfStatusWrapper.containsKey(step.CMPL123__WF_Rule__r.CMPL123__Target_Object__c)){
                tempStatusSet.addAll(wfStatusWrapper.get(step.CMPL123__WF_Rule__r.CMPL123__Target_Object__c));
            }
            if (step.CMPL123__Permission_Group__c != null && step.CMPL123__Permission_Group__c.contains('$')) {
                tempList.add(removeSpecialCharacter(step.CMPL123__Permission_Group__c));
            }
            // System.debug(step.CMPL123__Permission_Group__c);
            tempStatusSet.add(step.CMPL123__End_Status__r.Name);
            tempStatusSet.add(step.CMPL123__Start_Status__r.Name);

            wfRuleWrapper.put(step.CMPL123__WF_Rule__r.CMPL123__Target_Object__c, tempList);
            wfStatusWrapper.put(step.CMPL123__WF_Rule__r.CMPL123__Target_Object__c, tempStatusSet);
        }


        // doing this incase of migration of wf rules whose objects don't exist 
        List < EntityDefinition > objectList = [SELECT QualifiedApiName, Label
            FROM EntityDefinition
            WHERE IsRetrieveable = TRUE
            AND IsTriggerable = TRUE
            AND IsQueryable = TRUE
            AND IsEverUpdatable = TRUE
            AND IsSearchable = TRUE
            AND IsEverCreatable = TRUE
            AND IsCustomSetting = FALSE
            AND IsApexTriggerable = TRUE
            AND QualifiedApiName IN: test1
            ORDER BY Label ASC
        ];
        for (EntityDefinition entity: objectList) {
            wfWrapper wrapp = new wfWrapper();
            wrapp.apiName = entity.QualifiedApiName;
            wrapp.label = entity.label;
            wfObjects.add(wrapp);
            List < String > temp = new List < String > ();
            Set < String > tempSet = new Set < String > ();
            if (wfRuleWrapper.containsKey(entity.QualifiedApiName)) {
                temp.addAll(wfRuleWrapper.get(entity.QualifiedApiName));
            }
            if (wfStatusWrapper.containsKey(entity.QualifiedApiName)) {
                tempSet.addAll(wfStatusWrapper.get(entity.QualifiedApiName));
            }

            objectFieldMapping.put(entity.QualifiedApiName, temp);
            objectWfMapping.put(entity.QualifiedApiName, tempSet);
        
        }
        System.debug('objectWfMapping: ' + objectWfMapping);
        JsonMap = JSON.serialize(objectFieldMapping);
        wfStatusMap = JSON.serialize(objectWfMapping);

    }

    public PageReference passValue() {
        this.selectedApi = Apexpages.currentPage().getParameters().get('val');
        return null;
    }

    @TestVisible
    private String removeSpecialCharacter(String rawPermission){
        if( rawPermission.contains('$')){
            return rawPermission.remove('$');
        }
        return rawPermission;
    }

   public Pagereference goHome(){
        PageReference pageRef = new PageReference('/home/home.jsp');
        pageRef.setRedirect(true);
        return pageRef;
    }

    public String retrieveReportRecords(String reportName){
        reportTypeConfig = [select id, Field_Config_JSON__c, Report_Name__c, Object_Name__c, Workflow_Status_Config_JSON__c from Global_Assignee_Report_Setup__c where Report_Name__c =: reportName];
        return JSON.serialize(reportTypeConfig);
    }
}
/**
* @author Sparta Systems
* @date 2019
*
* @group Utility
* @group-content ../../ApexDocumentation/SoqlQuery.html
*
* @description Utility Class to dynamically generate SOQL Queries
*/
public class SoqlQuery {

     /**
    * @description selectionCriteria a list containing the desired fields pulled from the query.
    */
    public List<String> selectionCriteria {get;set;} 

     /**
    * @description objectApiName the object used for the query.
    */
    public String objectApiName {get;set;}

     /**
    * @description whereCriteria a map containing each field as key in the map with the where criteria list as a value. 
    */
    public Map<String,List<String>> whereCriteria {get;set;}

      /**
    * @description objectApiName the object used for the query.
    */
    public Boolean isNestedWhereClause {get;set;}

     /**
    * @description whereCriteria a map containing each field as key in the map with the where criteria list as a value. 
    */
    public List<WhereCriterium> whereCriteriaList {get;set;}

    /**
    * @description Default Constructor
    */
    public SoqlQuery(){
        this.selectionCriteria = new List<String>();
        this.objectApiName = '';
        this.whereCriteria = new Map<String,List<String>>();
        this.isNestedWhereClause = false;
    }

    public class WhereCriterium {
        public String fieldApiName {get;set;}
        public WhereTuple whereTuple {get;set;}
    }

    public class WhereTuple {
        public Boolean isNegation {get;set;}
        public List<String> whereInList {get;set;}
    }

    public SoqlQuery(List<String> selectionCriteria, String objectApiName, List<WhereCriterium> whereCriteriaList){
        this.selectionCriteria = selectionCriteria;
        this.objectApiName = objectApiName;
        this.whereCriteriaList = whereCriteriaList;
    }

    public SoqlQuery(List<String> selectionCriteria, String objectApiName,  Map<String,List<String>> whereCriteria, Boolean isNestedWhereClause){
        this.selectionCriteria = selectionCriteria;
        this.objectApiName = objectApiName;
        this.whereCriteria = whereCriteria;
        this.isNestedWhereClause = isNestedWhereClause;
    }

    public String generateBasicSoqlQuery(){
        List<String> selCriteria = this.selectionCriteria;
        String fullSelCriteriaString = String.join(selCriteria,',');
       
        List<String> fullWhereCriteriaList = new List<String>();

        for ( WhereCriterium whereCriterium : this.whereCriteriaList) {
            Integer whereCriteriaCount = 0;
            String selCriteriaString = '';
            Integer numWhereCriteria = whereCriterium.whereTuple.whereInList.size();
            List<String> whereInList = whereCriterium.whereTuple.whereInList;
            for ( String singleCriteria : whereInList) {
                whereCriteriaCount++;
                if ( singleCriteria.trim() != 'NULL') { 
                    selCriteriaString += '\'' + singleCriteria + '\'';
                }
                else {
                    selCriteriaString += singleCriteria;
                }
                if ( whereCriteriaCount != numWhereCriteria) {
                    selCriteriaString += ', ';
                }
            }
            String whereCriteriaString = '';
            if ( numWhereCriteria > 1) { 
                String equalityOperator = whereCriterium.whereTuple.isNegation == true ? ' NOT ' : '';
                whereCriteriaString = whereCriterium.fieldApiName + equalityOperator + ' IN ('+ selCriteriaString +')';
            }
            else { 
                String equalityOperator = whereCriterium.whereTuple.isNegation == true ? ' != ' : ' = ';
                whereCriteriaString = whereCriterium.fieldApiName + equalityOperator + selCriteriaString;
            }
            fullWhereCriteriaList.add(whereCriteriaString);
        }

        String whereCriteriaString = !fullWhereCriteriaList.isEmpty() ? ' WHERE ' + String.join(fullWhereCriteriaList, ' AND ') : '';
        return 'SELECT ' + fullSelCriteriaString + ' FROM ' + this.objectApiName + ' ' + whereCriteriaString;
    }

    public static List<SoqlQuery.WhereCriterium> generateWhereUsedCritieraForJdeWrapperRetrieval(String recordId, String whereField1, String whereField2){
        List<SoqlQuery.WhereCriterium> whereCriteriumList = new List<SoqlQuery.WhereCriterium>();
        SoqlQuery.WhereCriterium whereCriterium1 = new SoqlQuery.WhereCriterium();
        SoqlQuery.WhereCriterium whereCriterium2 = new SoqlQuery.WhereCriterium();
        SoqlQuery.WhereTuple whereTuple1 = new SoqlQuery.WhereTuple();
        SoqlQuery.WhereTuple whereTuple2 = new SoqlQuery.WhereTuple();
        List<String> whereInList1 = new List<String>();
        List<String> whereInList2 = new List<String>();
        String whereInId = recordId;

        whereCriterium1.fieldApiName = whereField1;
        whereTuple1.isNegation = false;
        whereInList1.add(recordId);
        whereTuple1.whereInList = whereInList1;
        whereCriterium1.whereTuple = whereTuple1;
        whereCriteriumList.add(whereCriterium1);

        whereCriterium2.fieldApiName = whereField2;
        whereTuple2.isNegation = true;
        whereInList2.add(' NULL ');
        whereTuple2.whereInList = whereInList2;
        whereCriterium2.whereTuple = whereTuple2;
        whereCriteriumList.add(whereCriterium2);
        return whereCriteriumList;
    }

    public override String toString(){
        List<String> selCriteria = this.selectionCriteria;
        String fullSelCriteriaString = String.join(selCriteria,',');

        List<String> fullWhereCriteriaList = new List<String>();

        for (String key : this.whereCriteria.keySet()) {
            if ( this.whereCriteria.containsKey(key)) {
                Integer whereCriteriaCount = 0;
                Integer numWhereCriteria = this.whereCriteria.get(key).size();
                List<String> whereCriteriaList = new List<String>();
                whereCriteriaList = whereCriteria.get(key);
                String selCriteriaString = '';

                for ( String singleCriteria : this.whereCriteria.get(key)) {
                    whereCriteriaCount++;
                    if ( !this.isNestedWhereClause) {
                        selCriteriaString += '\'' + singleCriteria + '\'';
                    }
                    else { 
                        selCriteriaString += singleCriteria;
                    }
                    if ( whereCriteriaCount != numWhereCriteria) {
                        selCriteriaString += ', ';
                    }
                    System.debug('where: ' + whereCriteriaCount);
                    System.debug('num: ' + numWhereCriteria);
                }
                String whereCriteriaString = key + ' IN ('+ selCriteriaString +')';
                fullWhereCriteriaList.add(whereCriteriaString);

            }
        }

        String whereCriteriaString = !fullWhereCriteriaList.isEmpty() ? ' WHERE ' + String.join(fullWhereCriteriaList, ' AND ') : '';

        return 'SELECT ' + fullSelCriteriaString + ' FROM ' + this.objectApiName + ' ' + whereCriteriaString;
    }
}